﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SistemaBanco
{
    public partial class FormBuscaCliente : Form
    {
        List<Cliente> clientes;
        FormConta novaConta;
        Cliente novoCliente;

        public FormBuscaCliente(Cliente novoCliente, List<Cliente> clientes, FormConta formPai)
        {
            InitializeComponent();
            this.clientes = clientes;
            novaConta = formPai;
            this.novoCliente = novoCliente;
            gridResultadoBusca.ReadOnly = true;
        }

        private void FormBuscaCliente_Load(object sender, EventArgs e)
        {
            

        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {
            gridResultadoBusca.Rows.Clear();
            List<Cliente> clienteBusca = new List<Cliente>();

            gridResultadoBusca.Visible = true;


            foreach (Cliente c in clientes)
            {
                if (c.getNome().Contains(txtNomeNovoCliente.Text))
                {
                    clienteBusca.Add(c);
                }
            }
           

            foreach (Cliente c in clienteBusca)
            {

                gridResultadoBusca.Rows.Add(c.getNome(), c.getTelefone(), c.getEndereco());
            }
        }

		private void gridResultadoBusca_CellContentClick(object sender, DataGridViewCellEventArgs e)
		{
			novaConta.txtBusca.Text = gridResultadoBusca.CurrentRow.Cells[0].Value.ToString();
			novoCliente.setNome(novaConta.txtBusca.Text);

			this.Close();
		}
	}
}
