﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SistemaBanco
{
    public partial class FormCliente : Form
    {
		// declaração do vetor(lista) de clientes
		List<Cliente> clientes;

		// construtor da classe clientes
        public FormCliente(List<Cliente> clientes)
        {
            InitializeComponent();
            this.clientes = clientes;
        }

		// salvando informações na lista
        private void button1_Click(object sender, EventArgs e)
        {
            Cliente c = new Cliente();

            c.setNome(txtNome.Text);
            c.setTelefone(txtTelefone.Text);
            c.setEndereco(txtEndereco.Text);

            clientes.Add(c);

            MessageBox.Show("Cliente salvo com sucesso", "Sucesso", MessageBoxButtons.OK);
            limparCampos();
        }

		//função
        public void limparCampos()
        {
            txtEndereco.Text = "";
            txtNome.Text = "";
            txtTelefone.Text = "";
        }

		private void txtNome_TextChanged(object sender, EventArgs e)
		{

		}
	}
}
