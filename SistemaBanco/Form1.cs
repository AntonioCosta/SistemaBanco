﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SistemaBanco
{
    public partial class FormPrincipal : Form
    {
        List<Cliente> clientes;
        List<Conta> contas;

        public FormPrincipal()
        {
            InitializeComponent();
            clientes = new List<Cliente>();
            contas = new List<Conta>();
        }

        private void sairToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if(DialogResult.Yes == MessageBox.Show("Deseja finalizar a aplicação?", "Confirmação", MessageBoxButtons.YesNo))
            {
                this.Close();
            }
        }

        private void novoToolStripMenuItem_Click(object sender, EventArgs e)
        {
            FormCliente formCliente = new FormCliente(clientes);
            formCliente.Show();
        }

        private void novaContaToolStripMenuItem_Click(object sender, EventArgs e)
        {
            FormConta formConta = new FormConta(clientes, contas);
            formConta.Show();

        }

        private void consultarToolStripMenuItem_Click(object sender, EventArgs e)
        {
            groupBox1.Visible = true;
            
        }

        private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void btBuscaCliente_Click(object sender, EventArgs e)
        {
            List<Cliente> clientesBusca = new List<Cliente>();

            foreach (Cliente c in clientes)
            {
                if (c.getNome().Contains(txtBusca.Text))
                {
                    clientesBusca.Add(c);
                }
            }

            if (clientesBusca.Count() == 0)
            {
                lblNenhumRegistro.Visible = true;
                gridCliente.Visible = false;
            }

            else
            {
                gridCliente.Visible = true;
                lblNenhumRegistro.Visible = false;
            }
           // gridCliente.Visible = true;
            

            foreach (Cliente c in clientesBusca)
            {

                gridCliente.Rows.Add(c.getNome(), c.getTelefone(), c.getEndereco());
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            txtBusca.Text = "";
            gridCliente.Rows.Clear();
        }

        private void label2_Click(object sender, EventArgs e)
        {

        }

        private void txtNomeCliente_TextChanged(object sender, EventArgs e)
        {

        }

		private void FormPrincipal_Load(object sender, EventArgs e)
		{

		}

		private void saqueToolStripMenuItem_Click(object sender, EventArgs e)
		{
			FormOperacao form = new FormOperacao("Saque", contas);
			form.Show();
		}

		private void depósitoToolStripMenuItem_Click(object sender, EventArgs e)
		{
			FormOperacao form = new FormOperacao("Deposito", contas);
			form.Show();
		}

		private void verificarSaldoToolStripMenuItem_Click(object sender, EventArgs e)
		{
			FormVerificaSaldo form = new FormVerificaSaldo(contas);
			form.Show();
		}
	}
}
