﻿namespace SistemaBanco
{
	partial class FormOperacao
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.lblNumeroDaConta = new System.Windows.Forms.Label();
			this.txtNumeroDaConta = new System.Windows.Forms.TextBox();
			this.txtSenha = new System.Windows.Forms.TextBox();
			this.label2 = new System.Windows.Forms.Label();
			this.txtValor = new System.Windows.Forms.TextBox();
			this.label3 = new System.Windows.Forms.Label();
			this.btOperacao = new System.Windows.Forms.Button();
			this.btCancelarOperacao = new System.Windows.Forms.Button();
			this.SuspendLayout();
			// 
			// lblNumeroDaConta
			// 
			this.lblNumeroDaConta.AutoSize = true;
			this.lblNumeroDaConta.Location = new System.Drawing.Point(138, 70);
			this.lblNumeroDaConta.Name = "lblNumeroDaConta";
			this.lblNumeroDaConta.Size = new System.Drawing.Size(93, 13);
			this.lblNumeroDaConta.TabIndex = 0;
			this.lblNumeroDaConta.Text = "Número da Conta:";
			// 
			// txtNumeroDaConta
			// 
			this.txtNumeroDaConta.Location = new System.Drawing.Point(231, 67);
			this.txtNumeroDaConta.Name = "txtNumeroDaConta";
			this.txtNumeroDaConta.Size = new System.Drawing.Size(196, 20);
			this.txtNumeroDaConta.TabIndex = 1;
			this.txtNumeroDaConta.TextChanged += new System.EventHandler(this.textBox1_TextChanged);
			// 
			// txtSenha
			// 
			this.txtSenha.Location = new System.Drawing.Point(231, 109);
			this.txtSenha.Name = "txtSenha";
			this.txtSenha.Size = new System.Drawing.Size(196, 20);
			this.txtSenha.TabIndex = 3;
			// 
			// label2
			// 
			this.label2.AutoSize = true;
			this.label2.Location = new System.Drawing.Point(190, 112);
			this.label2.Name = "label2";
			this.label2.Size = new System.Drawing.Size(41, 13);
			this.label2.TabIndex = 2;
			this.label2.Text = "Senha:";
			// 
			// txtValor
			// 
			this.txtValor.Location = new System.Drawing.Point(231, 154);
			this.txtValor.Name = "txtValor";
			this.txtValor.Size = new System.Drawing.Size(196, 20);
			this.txtValor.TabIndex = 5;
			// 
			// label3
			// 
			this.label3.AutoSize = true;
			this.label3.Location = new System.Drawing.Point(197, 157);
			this.label3.Name = "label3";
			this.label3.Size = new System.Drawing.Size(34, 13);
			this.label3.TabIndex = 4;
			this.label3.Text = "Valor:";
			// 
			// btOperacao
			// 
			this.btOperacao.Location = new System.Drawing.Point(200, 243);
			this.btOperacao.Name = "btOperacao";
			this.btOperacao.Size = new System.Drawing.Size(75, 23);
			this.btOperacao.TabIndex = 6;
			this.btOperacao.Text = "Operação";
			this.btOperacao.UseVisualStyleBackColor = true;
			this.btOperacao.Click += new System.EventHandler(this.btOperacao_Click);
			// 
			// btCancelarOperacao
			// 
			this.btCancelarOperacao.Location = new System.Drawing.Point(315, 243);
			this.btCancelarOperacao.Name = "btCancelarOperacao";
			this.btCancelarOperacao.Size = new System.Drawing.Size(75, 23);
			this.btCancelarOperacao.TabIndex = 7;
			this.btCancelarOperacao.Text = "Cancelar";
			this.btCancelarOperacao.UseVisualStyleBackColor = true;
			this.btCancelarOperacao.Click += new System.EventHandler(this.btCancelarOperacao_Click);
			// 
			// FormOperacao
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(584, 404);
			this.Controls.Add(this.btCancelarOperacao);
			this.Controls.Add(this.btOperacao);
			this.Controls.Add(this.txtValor);
			this.Controls.Add(this.label3);
			this.Controls.Add(this.txtSenha);
			this.Controls.Add(this.label2);
			this.Controls.Add(this.txtNumeroDaConta);
			this.Controls.Add(this.lblNumeroDaConta);
			this.Name = "FormOperacao";
			this.Text = "Operação";
			this.Load += new System.EventHandler(this.FormOperacao_Load);
			this.ResumeLayout(false);
			this.PerformLayout();

		}

		#endregion

		private System.Windows.Forms.Label lblNumeroDaConta;
		private System.Windows.Forms.TextBox txtNumeroDaConta;
		private System.Windows.Forms.TextBox txtSenha;
		private System.Windows.Forms.Label label2;
		private System.Windows.Forms.TextBox txtValor;
		private System.Windows.Forms.Label label3;
		private System.Windows.Forms.Button btOperacao;
		private System.Windows.Forms.Button btCancelarOperacao;
	}
}