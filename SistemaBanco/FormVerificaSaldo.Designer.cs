﻿namespace SistemaBanco
{
	partial class FormVerificaSaldo
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.label1 = new System.Windows.Forms.Label();
			this.label2 = new System.Windows.Forms.Label();
			this.txtNumeroConta = new System.Windows.Forms.TextBox();
			this.txtSenha = new System.Windows.Forms.TextBox();
			this.VerificaSaldo = new System.Windows.Forms.Button();
			this.labelSaldo = new System.Windows.Forms.Label();
			this.SuspendLayout();
			// 
			// label1
			// 
			this.label1.AutoSize = true;
			this.label1.Location = new System.Drawing.Point(81, 47);
			this.label1.Name = "label1";
			this.label1.Size = new System.Drawing.Size(93, 13);
			this.label1.TabIndex = 0;
			this.label1.Text = "Número da Conta:";
			// 
			// label2
			// 
			this.label2.AutoSize = true;
			this.label2.Location = new System.Drawing.Point(133, 91);
			this.label2.Name = "label2";
			this.label2.Size = new System.Drawing.Size(41, 13);
			this.label2.TabIndex = 1;
			this.label2.Text = "Senha:";
			// 
			// txtNumeroConta
			// 
			this.txtNumeroConta.Location = new System.Drawing.Point(171, 44);
			this.txtNumeroConta.Name = "txtNumeroConta";
			this.txtNumeroConta.Size = new System.Drawing.Size(196, 20);
			this.txtNumeroConta.TabIndex = 2;
			// 
			// txtSenha
			// 
			this.txtSenha.Location = new System.Drawing.Point(171, 88);
			this.txtSenha.Name = "txtSenha";
			this.txtSenha.Size = new System.Drawing.Size(196, 20);
			this.txtSenha.TabIndex = 3;
			// 
			// VerificaSaldo
			// 
			this.VerificaSaldo.Location = new System.Drawing.Point(171, 180);
			this.VerificaSaldo.Name = "VerificaSaldo";
			this.VerificaSaldo.Size = new System.Drawing.Size(83, 23);
			this.VerificaSaldo.TabIndex = 4;
			this.VerificaSaldo.Text = "Verificar Saldo";
			this.VerificaSaldo.UseVisualStyleBackColor = true;
			this.VerificaSaldo.Click += new System.EventHandler(this.VerificaSaldo_Click);
			// 
			// labelSaldo
			// 
			this.labelSaldo.AutoSize = true;
			this.labelSaldo.Location = new System.Drawing.Point(260, 185);
			this.labelSaldo.Name = "labelSaldo";
			this.labelSaldo.Size = new System.Drawing.Size(45, 13);
			this.labelSaldo.TabIndex = 5;
			this.labelSaldo.Text = "R$ 0,00";
			// 
			// FormVerificaSaldo
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(475, 327);
			this.Controls.Add(this.labelSaldo);
			this.Controls.Add(this.VerificaSaldo);
			this.Controls.Add(this.txtSenha);
			this.Controls.Add(this.txtNumeroConta);
			this.Controls.Add(this.label2);
			this.Controls.Add(this.label1);
			this.Name = "FormVerificaSaldo";
			this.Text = "Ferifica Saldo";
			this.ResumeLayout(false);
			this.PerformLayout();

		}

		#endregion

		private System.Windows.Forms.Label label1;
		private System.Windows.Forms.Label label2;
		private System.Windows.Forms.TextBox txtNumeroConta;
		private System.Windows.Forms.TextBox txtSenha;
		private System.Windows.Forms.Button VerificaSaldo;
		private System.Windows.Forms.Label labelSaldo;
	}
}