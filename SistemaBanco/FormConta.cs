﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SistemaBanco
{
    public partial class FormConta : Form
    {
        Cliente novoCliente;
        List<Cliente> clientes;

		//Conta novaConta;
		List<Conta> contas;

		public FormConta(List<Cliente> clientes, List<Conta> contas)
        {
            InitializeComponent();
            this.clientes = clientes;

			novoCliente = new Cliente();

			this.contas = contas;
			//novaConta = new Conta();
		}

        private void button2_Click(object sender, EventArgs e)
        {
            FormBuscaCliente busca = new FormBuscaCliente(novoCliente, clientes, this);
            busca.Show();
        }


		private void btCriarConta_Click(object sender, EventArgs e)
		{
			Conta conta = new Conta();

			conta.setCliente(novoCliente);
			conta.setNumero(Convert.ToInt16(txtNumeroConta.Text));
			if (!txtSenha.Text.Equals(txtConfirmarSenha.Text))
			{
				MessageBox.Show("Senha e Cofirmação de Senhas Diferentes", "Erro", MessageBoxButtons.OK);
				txtSenha.Text = "";
				txtConfirmarSenha.Text = "";
			}
			else
			{
				conta.setSenha(txtSenha.Text);
				conta.setSaldo(0);
				contas.Add(conta);
				MessageBox.Show("Nova Conta salva com sucesso", "Sucesso", MessageBoxButtons.OK);
				limparCampos();
			}
		}

		public void limparCampos()
		{
			txtNumeroConta.Text = "";
			txtSenha.Text = "";
			txtConfirmarSenha.Text = "";
		}

		private void txtNumeroConta_TextChanged(object sender, EventArgs e)
		{

		}
	}
}
