﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SistemaBanco
{
	public partial class FormOperacao : Form
	{
		List<Conta> contas;
		String operacao;

		public FormOperacao(String operacao, List<Conta> contas)
		{
			InitializeComponent();
			this.contas = contas;
			this.operacao = operacao;

			if (operacao.Equals("Saque"))
			{
				btOperacao.Text = "Sacar";
			}
			else
			{
				btOperacao.Text = "Depositar";
			}
		}

		private void FormOperacao_Load(object sender, EventArgs e)
		{

		}

		private void btOperacao_Click(object sender, EventArgs e)
		{
			int achouconta = 0;

			foreach(Conta c in contas)
			{
				if(c.getNumero() == Convert.ToInt16(txtNumeroDaConta.Text))
				{
					achouconta = 1;
					if(c.getSenha() == txtSenha.Text)
					{
						if (operacao.Equals("Saque"))
						{
							c.setSaldo(c.getSaldo() - Convert.ToDouble(txtValor.Text));
							MessageBox.Show("Saque efetuado com sucesso!", "Sucesso", MessageBoxButtons.OK);
						}
						else
						{
							c.setSaldo(c.getSaldo() + Convert.ToDouble(txtValor.Text));
							MessageBox.Show("Depósito efetuado com sucesso!", "Sucesso", MessageBoxButtons.OK);
						}
						break;
					}
				}
				else
				{
					MessageBox.Show("Senha Incorreta!", "Erro", MessageBoxButtons.OK);
					txtSenha.Text = "";
					break;
				}
			}
			if(achouconta == 0)
			{
				MessageBox.Show("Conta não encontrada!", "Erro", MessageBoxButtons.OK);
			}
		}

		private void btCancelarOperacao_Click(object sender, EventArgs e)
		{
			Close();
		}

		private void textBox1_TextChanged(object sender, EventArgs e)
		{

		}
	}
}
