﻿namespace SistemaBanco
{
    partial class FormBuscaCliente
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
			this.label1 = new System.Windows.Forms.Label();
			this.txtNomeNovoCliente = new System.Windows.Forms.TextBox();
			this.gridResultadoBusca = new System.Windows.Forms.DataGridView();
			this.Cliente = new System.Windows.Forms.DataGridViewTextBoxColumn();
			((System.ComponentModel.ISupportInitialize)(this.gridResultadoBusca)).BeginInit();
			this.SuspendLayout();
			// 
			// label1
			// 
			this.label1.AutoSize = true;
			this.label1.Location = new System.Drawing.Point(36, 22);
			this.label1.Name = "label1";
			this.label1.Size = new System.Drawing.Size(38, 13);
			this.label1.TabIndex = 0;
			this.label1.Text = "Nome:";
			// 
			// txtNomeNovoCliente
			// 
			this.txtNomeNovoCliente.Location = new System.Drawing.Point(39, 39);
			this.txtNomeNovoCliente.Name = "txtNomeNovoCliente";
			this.txtNomeNovoCliente.Size = new System.Drawing.Size(343, 20);
			this.txtNomeNovoCliente.TabIndex = 1;
			this.txtNomeNovoCliente.TextChanged += new System.EventHandler(this.textBox1_TextChanged);
			// 
			// gridResultadoBusca
			// 
			this.gridResultadoBusca.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
			this.gridResultadoBusca.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Cliente});
			this.gridResultadoBusca.Location = new System.Drawing.Point(12, 110);
			this.gridResultadoBusca.Name = "gridResultadoBusca";
			this.gridResultadoBusca.Size = new System.Drawing.Size(402, 291);
			this.gridResultadoBusca.TabIndex = 2;
			this.gridResultadoBusca.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.gridResultadoBusca_CellContentClick);
			// 
			// Cliente
			// 
			this.Cliente.HeaderText = "Cliente";
			this.Cliente.Name = "Cliente";
			this.Cliente.Width = 375;
			// 
			// FormBuscaCliente
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(426, 413);
			this.Controls.Add(this.gridResultadoBusca);
			this.Controls.Add(this.txtNomeNovoCliente);
			this.Controls.Add(this.label1);
			this.Name = "FormBuscaCliente";
			this.Text = "FormBuscaCliente";
			this.Load += new System.EventHandler(this.FormBuscaCliente_Load);
			((System.ComponentModel.ISupportInitialize)(this.gridResultadoBusca)).EndInit();
			this.ResumeLayout(false);
			this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox txtNomeNovoCliente;
        private System.Windows.Forms.DataGridView gridResultadoBusca;
        private System.Windows.Forms.DataGridViewTextBoxColumn Cliente;
    }
}