﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SistemaBanco
{
	public partial class FormVerificaSaldo : Form
	{
		List<Conta> contas;
		public FormVerificaSaldo(List<Conta> contas)
		{
			InitializeComponent();
			this.contas = contas;
		}

		private void VerificaSaldo_Click(object sender, EventArgs e)
		{
			int achouconta = 0;

			foreach (Conta c in contas)
			{
				if(c.getNumero() == Convert.ToInt16(txtNumeroConta.Text))
				{
					achouconta = 1;
					if (c.getSenha() == txtSenha.Text)
					{
						labelSaldo.Text = Convert.ToString("R$ "+c.getSaldo());
					}
					else
					{
						MessageBox.Show("Senha Incorreta!", "Erro", MessageBoxButtons.OK);
						txtSenha.Text = "";
						break;
					}
				}
			}
			if (achouconta == 0)
			{
				MessageBox.Show("Conta não encontrada!", "Erro", MessageBoxButtons.OK);
			}
		}
	}
}