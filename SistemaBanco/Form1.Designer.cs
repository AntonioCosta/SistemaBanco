﻿namespace SistemaBanco
{
    partial class FormPrincipal
    {
        /// <summary>
        /// Variável de designer necessária.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Limpar os recursos que estão sendo usados.
        /// </summary>
        /// <param name="disposing">true se for necessário descartar os recursos gerenciados; caso contrário, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código gerado pelo Windows Form Designer

        /// <summary>
        /// Método necessário para suporte ao Designer - não modifique 
        /// o conteúdo deste método com o editor de código.
        /// </summary>
        private void InitializeComponent()
        {
			this.menuStrip1 = new System.Windows.Forms.MenuStrip();
			this.clienteToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.novoToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.consultarToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.contaToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.novaContaToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.saqueToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.depósitoToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.verificarSaldoToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.sairToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.groupBox1 = new System.Windows.Forms.GroupBox();
			this.lblNenhumRegistro = new System.Windows.Forms.Label();
			this.btBuscaCliente = new System.Windows.Forms.Button();
			this.button1 = new System.Windows.Forms.Button();
			this.gridCliente = new System.Windows.Forms.DataGridView();
			this.Nome = new System.Windows.Forms.DataGridViewTextBoxColumn();
			this.Column1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
			this.Column2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
			this.txtBusca = new System.Windows.Forms.TextBox();
			this.label1 = new System.Windows.Forms.Label();
			this.backgroundWorker1 = new System.ComponentModel.BackgroundWorker();
			this.menuStrip1.SuspendLayout();
			this.groupBox1.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.gridCliente)).BeginInit();
			this.SuspendLayout();
			// 
			// menuStrip1
			// 
			this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.clienteToolStripMenuItem,
            this.contaToolStripMenuItem,
            this.sairToolStripMenuItem});
			this.menuStrip1.Location = new System.Drawing.Point(0, 0);
			this.menuStrip1.Name = "menuStrip1";
			this.menuStrip1.Size = new System.Drawing.Size(708, 24);
			this.menuStrip1.TabIndex = 0;
			this.menuStrip1.Text = "menuStrip1";
			// 
			// clienteToolStripMenuItem
			// 
			this.clienteToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.novoToolStripMenuItem,
            this.consultarToolStripMenuItem});
			this.clienteToolStripMenuItem.Name = "clienteToolStripMenuItem";
			this.clienteToolStripMenuItem.Size = new System.Drawing.Size(56, 20);
			this.clienteToolStripMenuItem.Text = "Cliente";
			// 
			// novoToolStripMenuItem
			// 
			this.novoToolStripMenuItem.Name = "novoToolStripMenuItem";
			this.novoToolStripMenuItem.Size = new System.Drawing.Size(180, 22);
			this.novoToolStripMenuItem.Text = "Novo";
			this.novoToolStripMenuItem.Click += new System.EventHandler(this.novoToolStripMenuItem_Click);
			// 
			// consultarToolStripMenuItem
			// 
			this.consultarToolStripMenuItem.Name = "consultarToolStripMenuItem";
			this.consultarToolStripMenuItem.Size = new System.Drawing.Size(180, 22);
			this.consultarToolStripMenuItem.Text = "Consultar";
			this.consultarToolStripMenuItem.Click += new System.EventHandler(this.consultarToolStripMenuItem_Click);
			// 
			// contaToolStripMenuItem
			// 
			this.contaToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.novaContaToolStripMenuItem,
            this.saqueToolStripMenuItem,
            this.depósitoToolStripMenuItem,
            this.verificarSaldoToolStripMenuItem});
			this.contaToolStripMenuItem.Name = "contaToolStripMenuItem";
			this.contaToolStripMenuItem.Size = new System.Drawing.Size(51, 20);
			this.contaToolStripMenuItem.Text = "Conta";
			// 
			// novaContaToolStripMenuItem
			// 
			this.novaContaToolStripMenuItem.Name = "novaContaToolStripMenuItem";
			this.novaContaToolStripMenuItem.Size = new System.Drawing.Size(149, 22);
			this.novaContaToolStripMenuItem.Text = "Nova Conta";
			this.novaContaToolStripMenuItem.Click += new System.EventHandler(this.novaContaToolStripMenuItem_Click);
			// 
			// saqueToolStripMenuItem
			// 
			this.saqueToolStripMenuItem.Name = "saqueToolStripMenuItem";
			this.saqueToolStripMenuItem.Size = new System.Drawing.Size(149, 22);
			this.saqueToolStripMenuItem.Text = "Saque";
			this.saqueToolStripMenuItem.Click += new System.EventHandler(this.saqueToolStripMenuItem_Click);
			// 
			// depósitoToolStripMenuItem
			// 
			this.depósitoToolStripMenuItem.Name = "depósitoToolStripMenuItem";
			this.depósitoToolStripMenuItem.Size = new System.Drawing.Size(149, 22);
			this.depósitoToolStripMenuItem.Text = "Depósito";
			this.depósitoToolStripMenuItem.Click += new System.EventHandler(this.depósitoToolStripMenuItem_Click);
			// 
			// verificarSaldoToolStripMenuItem
			// 
			this.verificarSaldoToolStripMenuItem.Name = "verificarSaldoToolStripMenuItem";
			this.verificarSaldoToolStripMenuItem.Size = new System.Drawing.Size(149, 22);
			this.verificarSaldoToolStripMenuItem.Text = "Verificar Saldo";
			this.verificarSaldoToolStripMenuItem.Click += new System.EventHandler(this.verificarSaldoToolStripMenuItem_Click);
			// 
			// sairToolStripMenuItem
			// 
			this.sairToolStripMenuItem.Name = "sairToolStripMenuItem";
			this.sairToolStripMenuItem.Size = new System.Drawing.Size(38, 20);
			this.sairToolStripMenuItem.Text = "Sair";
			this.sairToolStripMenuItem.Click += new System.EventHandler(this.sairToolStripMenuItem_Click);
			// 
			// groupBox1
			// 
			this.groupBox1.Controls.Add(this.lblNenhumRegistro);
			this.groupBox1.Controls.Add(this.btBuscaCliente);
			this.groupBox1.Controls.Add(this.button1);
			this.groupBox1.Controls.Add(this.gridCliente);
			this.groupBox1.Controls.Add(this.txtBusca);
			this.groupBox1.Controls.Add(this.label1);
			this.groupBox1.Location = new System.Drawing.Point(27, 59);
			this.groupBox1.Name = "groupBox1";
			this.groupBox1.Size = new System.Drawing.Size(648, 313);
			this.groupBox1.TabIndex = 1;
			this.groupBox1.TabStop = false;
			this.groupBox1.Text = "Buscar Cliente";
			this.groupBox1.Visible = false;
			// 
			// lblNenhumRegistro
			// 
			this.lblNenhumRegistro.AutoSize = true;
			this.lblNenhumRegistro.Location = new System.Drawing.Point(156, 99);
			this.lblNenhumRegistro.Name = "lblNenhumRegistro";
			this.lblNenhumRegistro.Size = new System.Drawing.Size(144, 13);
			this.lblNenhumRegistro.TabIndex = 5;
			this.lblNenhumRegistro.Text = "Nenhum registro encontrado!";
			this.lblNenhumRegistro.Click += new System.EventHandler(this.label2_Click);
			// 
			// btBuscaCliente
			// 
			this.btBuscaCliente.Location = new System.Drawing.Point(448, 62);
			this.btBuscaCliente.Name = "btBuscaCliente";
			this.btBuscaCliente.Size = new System.Drawing.Size(75, 23);
			this.btBuscaCliente.TabIndex = 4;
			this.btBuscaCliente.Text = "Buscar";
			this.btBuscaCliente.UseVisualStyleBackColor = true;
			this.btBuscaCliente.Click += new System.EventHandler(this.btBuscaCliente_Click);
			// 
			// button1
			// 
			this.button1.Location = new System.Drawing.Point(357, 62);
			this.button1.Name = "button1";
			this.button1.Size = new System.Drawing.Size(75, 23);
			this.button1.TabIndex = 3;
			this.button1.Text = "Limpar";
			this.button1.UseVisualStyleBackColor = true;
			this.button1.Click += new System.EventHandler(this.button1_Click);
			// 
			// gridCliente
			// 
			this.gridCliente.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
			this.gridCliente.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Nome,
            this.Column1,
            this.Column2});
			this.gridCliente.Location = new System.Drawing.Point(18, 132);
			this.gridCliente.Name = "gridCliente";
			this.gridCliente.Size = new System.Drawing.Size(610, 165);
			this.gridCliente.TabIndex = 2;
			this.gridCliente.Visible = false;
			this.gridCliente.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridView1_CellContentClick);
			// 
			// Nome
			// 
			this.Nome.HeaderText = "Nome";
			this.Nome.Name = "Nome";
			this.Nome.Width = 250;
			// 
			// Column1
			// 
			this.Column1.HeaderText = "Telefone";
			this.Column1.Name = "Column1";
			// 
			// Column2
			// 
			this.Column2.HeaderText = "Endereço";
			this.Column2.Name = "Column2";
			this.Column2.Width = 225;
			// 
			// txtBusca
			// 
			this.txtBusca.Location = new System.Drawing.Point(156, 36);
			this.txtBusca.Name = "txtBusca";
			this.txtBusca.Size = new System.Drawing.Size(367, 20);
			this.txtBusca.TabIndex = 1;
			this.txtBusca.TextChanged += new System.EventHandler(this.txtNomeCliente_TextChanged);
			// 
			// label1
			// 
			this.label1.AutoSize = true;
			this.label1.Location = new System.Drawing.Point(112, 39);
			this.label1.Name = "label1";
			this.label1.Size = new System.Drawing.Size(38, 13);
			this.label1.TabIndex = 0;
			this.label1.Text = "Nome:";
			// 
			// FormPrincipal
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(708, 402);
			this.Controls.Add(this.groupBox1);
			this.Controls.Add(this.menuStrip1);
			this.MainMenuStrip = this.menuStrip1;
			this.Name = "FormPrincipal";
			this.Text = "Formulário Banco";
			this.Load += new System.EventHandler(this.FormPrincipal_Load);
			this.menuStrip1.ResumeLayout(false);
			this.menuStrip1.PerformLayout();
			this.groupBox1.ResumeLayout(false);
			this.groupBox1.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.gridCliente)).EndInit();
			this.ResumeLayout(false);
			this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem clienteToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem contaToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem novaContaToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem saqueToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem sairToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem novoToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem consultarToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem depósitoToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem verificarSaldoToolStripMenuItem;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.TextBox txtBusca;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.DataGridView gridCliente;
        private System.Windows.Forms.DataGridViewTextBoxColumn Nome;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column1;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column2;
        private System.Windows.Forms.Button btBuscaCliente;
        private System.Windows.Forms.Button button1;
        private System.ComponentModel.BackgroundWorker backgroundWorker1;
        private System.Windows.Forms.Label lblNenhumRegistro;
    }
}

