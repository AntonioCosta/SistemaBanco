﻿namespace SistemaBanco
{
    partial class FormConta
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
			this.label1 = new System.Windows.Forms.Label();
			this.label2 = new System.Windows.Forms.Label();
			this.label3 = new System.Windows.Forms.Label();
			this.txtNumeroConta = new System.Windows.Forms.TextBox();
			this.txtSenha = new System.Windows.Forms.TextBox();
			this.txtConfirmarSenha = new System.Windows.Forms.TextBox();
			this.btCriarConta = new System.Windows.Forms.Button();
			this.button2 = new System.Windows.Forms.Button();
			this.label4 = new System.Windows.Forms.Label();
			this.txtBusca = new System.Windows.Forms.TextBox();
			this.button3 = new System.Windows.Forms.Button();
			this.SuspendLayout();
			// 
			// label1
			// 
			this.label1.AutoSize = true;
			this.label1.Location = new System.Drawing.Point(85, 106);
			this.label1.Name = "label1";
			this.label1.Size = new System.Drawing.Size(93, 13);
			this.label1.TabIndex = 0;
			this.label1.Text = "Número da Conta:";
			// 
			// label2
			// 
			this.label2.AutoSize = true;
			this.label2.Location = new System.Drawing.Point(137, 144);
			this.label2.Name = "label2";
			this.label2.Size = new System.Drawing.Size(41, 13);
			this.label2.TabIndex = 1;
			this.label2.Text = "Senha:";
			// 
			// label3
			// 
			this.label3.AutoSize = true;
			this.label3.Location = new System.Drawing.Point(93, 178);
			this.label3.Name = "label3";
			this.label3.Size = new System.Drawing.Size(85, 13);
			this.label3.TabIndex = 2;
			this.label3.Text = "Corfirmar Senha:";
			// 
			// txtNumeroConta
			// 
			this.txtNumeroConta.Location = new System.Drawing.Point(175, 103);
			this.txtNumeroConta.Name = "txtNumeroConta";
			this.txtNumeroConta.Size = new System.Drawing.Size(227, 20);
			this.txtNumeroConta.TabIndex = 3;
			this.txtNumeroConta.TextChanged += new System.EventHandler(this.txtNumeroConta_TextChanged);
			// 
			// txtSenha
			// 
			this.txtSenha.Location = new System.Drawing.Point(175, 141);
			this.txtSenha.Name = "txtSenha";
			this.txtSenha.PasswordChar = '*';
			this.txtSenha.Size = new System.Drawing.Size(227, 20);
			this.txtSenha.TabIndex = 4;
			// 
			// txtConfirmarSenha
			// 
			this.txtConfirmarSenha.Location = new System.Drawing.Point(175, 175);
			this.txtConfirmarSenha.Name = "txtConfirmarSenha";
			this.txtConfirmarSenha.PasswordChar = '*';
			this.txtConfirmarSenha.Size = new System.Drawing.Size(227, 20);
			this.txtConfirmarSenha.TabIndex = 5;
			// 
			// btCriarConta
			// 
			this.btCriarConta.Location = new System.Drawing.Point(202, 259);
			this.btCriarConta.Name = "btCriarConta";
			this.btCriarConta.Size = new System.Drawing.Size(75, 23);
			this.btCriarConta.TabIndex = 7;
			this.btCriarConta.Text = "Criar Conta";
			this.btCriarConta.UseVisualStyleBackColor = true;
			this.btCriarConta.Click += new System.EventHandler(this.btCriarConta_Click);
			// 
			// button2
			// 
			this.button2.Location = new System.Drawing.Point(444, 29);
			this.button2.Name = "button2";
			this.button2.Size = new System.Drawing.Size(29, 23);
			this.button2.TabIndex = 8;
			this.button2.Text = "...";
			this.button2.UseVisualStyleBackColor = true;
			this.button2.Click += new System.EventHandler(this.button2_Click);
			// 
			// label4
			// 
			this.label4.AutoSize = true;
			this.label4.Location = new System.Drawing.Point(82, 34);
			this.label4.Name = "label4";
			this.label4.Size = new System.Drawing.Size(42, 13);
			this.label4.TabIndex = 9;
			this.label4.Text = "Cliente:";
			// 
			// txtBusca
			// 
			this.txtBusca.Enabled = false;
			this.txtBusca.Location = new System.Drawing.Point(130, 31);
			this.txtBusca.Name = "txtBusca";
			this.txtBusca.Size = new System.Drawing.Size(308, 20);
			this.txtBusca.TabIndex = 10;
			// 
			// button3
			// 
			this.button3.Location = new System.Drawing.Point(293, 259);
			this.button3.Name = "button3";
			this.button3.Size = new System.Drawing.Size(75, 23);
			this.button3.TabIndex = 11;
			this.button3.Text = "Cancelar";
			this.button3.UseVisualStyleBackColor = true;
			// 
			// FormConta
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(555, 375);
			this.Controls.Add(this.button3);
			this.Controls.Add(this.txtBusca);
			this.Controls.Add(this.label4);
			this.Controls.Add(this.button2);
			this.Controls.Add(this.btCriarConta);
			this.Controls.Add(this.txtConfirmarSenha);
			this.Controls.Add(this.txtSenha);
			this.Controls.Add(this.txtNumeroConta);
			this.Controls.Add(this.label3);
			this.Controls.Add(this.label2);
			this.Controls.Add(this.label1);
			this.Name = "FormConta";
			this.Text = "FormConta";
			this.ResumeLayout(false);
			this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox txtNumeroConta;
        private System.Windows.Forms.TextBox txtSenha;
        private System.Windows.Forms.TextBox txtConfirmarSenha;
        private System.Windows.Forms.Button btCriarConta;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Label label4;
        public System.Windows.Forms.TextBox txtBusca;
        private System.Windows.Forms.Button button3;
    }
}