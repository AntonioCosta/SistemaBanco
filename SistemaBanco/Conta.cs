﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SistemaBanco
{
    public class Conta
    {
        private Cliente cliente;
        private String senha;
        private double saldo;
        private int numero;


        //Cliente
        public void setCliente(Cliente cliente)
        {
            this.cliente = cliente;
        }
        public Cliente getCliente()
        {
            return cliente;
        }
		//saldo
		public void setSaldo(double saldo)
        {
            this.saldo = saldo;
        }
        public double getSaldo()
        {
            return saldo;
        }
        //numero
        public void setNumero(int numero)
        {
            this.numero = numero;
        }
        public int getNumero()
        {
            return numero;
        }
		//senha
		public void setSenha(String senha)
		{
			this.senha = senha;
		}
		public String getSenha()
		{
			return senha;
		}
	}
}
